import { Controller, Get, Header, HttpCode } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @HttpCode(200)
  @Get()
  @Header('content-type', 'application/json')
  getData(): string {
    return this.appService.getData();
  }
}
