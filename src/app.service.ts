import { Injectable } from '@nestjs/common';
import data from './data';

@Injectable()
export class AppService {
  getData(): string {
    return JSON.stringify(data);
  }
}
